import base64
from jinja2 import Template
import settings
import argparse
import yaml
import os
import sys


def base64_string(filename):
    try:
        with open(filename, 'rb') as fin:
            picture = fin.read()
            encoded = base64.encodebytes(picture)
            decoded = encoded.decode('utf-8').replace('\n', '')
            return decoded
    except:
        raise


def render_template(content):
    try:
        content['front_imagetype'] = content.get('front_image', 'jpg')[-3:]
        if content.get('content_font', None) is None:
            content['content_font'] = base64_string(settings.CONTENT_FONT)
        content['style'] = open(settings.CSSFILE, 'r').read()

        content['front_image'] = base64_string(content['front_image'])
        content['stamp'] = base64_string(content['stamp'])
        content['title'] = content['title'].replace("\n", "<br/>")
        content['address'] = content['address'].replace("\n", "<br/>")
        content['content'] = content['content'].replace("\n", "<br/>")

        template = Template(open(settings.HTMLTEMPLATE, 'r').read())
        return template.render(content)
    except:
        raise


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create html postcards.")
    parser.add_argument("--yaml", help="YAML file with postcards definitions.")
    parser.add_argument("--output", help="Output directory for postcards. Defaults to '.'", default=".")

    args = parser.parse_args()
    if args.yaml is None:
        parser.print_help()
        sys.exit(1)

    postcards = yaml.safe_load(open(args.yaml, 'r'))
    for postcard in postcards:
        html = render_template(postcard)
        with(open(os.path.join(args.output, postcard['name'] + ".html"), 'w')) as fout:
            fout.write(html)
