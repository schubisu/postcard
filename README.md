# postcard

A convenient wrapper to create single-file html postcards.


The postcard has a front image and a title, address, and text field on the back. The backside is drawn with css and customizable via `postcard.css`. `postcard.html` is a `jinja2` template with placeholders for the text and image fields.
The whole content, including base64 encoded images and fonts, is compiled to a single html file.

Postcards can be defined in a yaml file, as demonstrated in `sample_cards.yaml`.

### Usage

```bash
usage: postcard.py [-h] [--yaml YAML] [--output OUTPUT]

Create html postcards.

optional arguments:
  -h, --help       show this help message and exit
  --yaml YAML      YAML file with postcards definitions.
  --output OUTPUT  Output directory for postcards. Defaults to '.'
```

TODO:
- interactive mode
