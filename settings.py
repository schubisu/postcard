import os

BASEDIR = os.path.dirname(os.path.abspath(__file__))
HTMLTEMPLATE = os.path.join(BASEDIR, "postcard.html")
CSSFILE = os.path.join(BASEDIR, "postcard.css")
CONTENT_FONT = os.path.join(BASEDIR, 'fonts', 'blackjack.ttf')

